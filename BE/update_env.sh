#!/bin/bash

# update backend port
file=".env"
search="BACKEND_PORT=8080"
replace="BACKEND_PORT=80"

# Check if the file exists
if [ -f "$file" ]; then
    # Use sed to replace the line
    sed -i "s/$search/$replace/" "$file"
    echo "Replacement complete. Updated $file"
else
    echo "Error: File $file not found."
fi


# update frontend port

file=".env"
search="FRONTEND_PORT=3000"
replace="FRONTEND_PORT=443"

# Check if the file exists
if [ -f "$file" ]; then
    # Use sed to replace the line
    sed -i "s/$search/$replace/" "$file"
    echo "Replacement complete. Updated $file"
else
    echo "Error: File $file not found."
fi


